.PHONY: dev
dev:
	@docker run -d --name slides-dev --rm -t -i -v $$(pwd):/usr/share/nginx/html -p 8888:80 nginx:1.13-alpine
	@x-www-browser localhost:8888

.PHONY: dev_stop
dev_stop:
	@docker stop slides-dev
