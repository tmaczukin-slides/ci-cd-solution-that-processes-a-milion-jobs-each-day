### The plan

1. Quick overview of GitLab CI <!-- .element: class="fragment" data-fragment-index="1" -->
1. The story of CI at GitLab.com <!-- .element: class="fragment" data-fragment-index="2" -->
1. Where are we now? <!-- .element: class="fragment" data-fragment-index="3" -->
1. Looking into the future <!-- .element: class="fragment" data-fragment-index="4" -->

---

## Quick overview of GitLab CI

---

### Quick overview of GitLab CI

#### General architecture before GitLab 8.0

![pre-8.0 general architecture diagram](img/general-architecture-pre-8-0.png)

+++

### Quick overview of GitLab CI

#### General architecture since GitLab 8.0

![post-8.0 general architecture diagram](img/general-architecture-post-8-0.png)

+++

### Quick overview of GitLab CI

#### More detailed architecture of GitLab CI

![more detailed GitLab architecture diagram](img/detailed-gitlab-architecture.png)

---

## The story of CI at GitLab.com

---

### The story of CI at GitLab.com

#### It started with few Shared Runners

- the initial fleet of static runners
- small GitLab.com fleet

![initial infrastructure fleet of GitLab.com](img/initial-infrastructure-at-2016.png)

---

### The story of CI at GitLab.com

#### Docker Machine autoscaling is born

- used Docker Machine to spin new machines when needed
- intruduction of _Runner Manager_ idea
- tests with private runners
- [performance issues](https://gitlab.com/gitlab-com/infrastructure/issues/18):
    - upgrade Token Rate Limit
    - reconfigure rsyslog - too much resources usage
    - implement [_fair usage scheduler_](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/4634)
    - [improve GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/200)

+++

### The story of CI at GitLab.com

#### Make all Shared Runners autoscaled

Autoscaled Shared Runners [were enabled publicly](https://gitlab.com/gitlab-com/infrastructure/issues/29#note_12524428)
on GitLab.com at 2016-04-05

![infrastructure when autoscaling was introduced](img/infrastructure-with-introduced-autoscaling.png)

---

### The story of CI at GitLab.com

#### Monitoring for the help

- check_mk - the initial monitoring solution
- first CI "metrics" and alerts:
    - [token rate limit](https://gitlab.com/gitlab-com/infrastructure/issues/4)
    - [machines in _error_ state](https://gitlab.com/gitlab-com/operations/issues/295)
    - number of existing machines
- [sentry for exceptions tracking](https://gitlab.com/gitlab-com/infrastructure/issues/173) added for Runners

+++

### The story of CI at GitLab.com

#### Monitoring for the help

![check_mk machines number graph](img/checkmk-machines-number-graph.png)

![check_mk alert example](img/checkmk-alert-example.png)

+++

### The story of CI at GitLab.com

#### Monitoring for the help

"If you say you know your software, but you don't measure it - you're wrong!"

---

### The story of CI at GitLab.com

#### The case of nasty _--privileged_ option

- [enable _--privileged_](https://gitlab.com/gitlab-com/infrastructure/issues/158)   for Shared Runners
- docker machine security [issue and fix](https://github.com/docker/machine/commit/c0b721b5f9d8ffcbcc1df15e20ba1fdb756c58f7)
- architecture decisions: _MaxBuilds = 1_ -> remove machine after each job

---

### The story of CI at GitLab.com

#### Docker Machine - it can bite

- problems with Docker Machine and DigitalOcean
- existing machines [cross checking and cleanup](https://gitlab.com/gitlab-com/infrastructure/issues/921)

+++

### The story of CI at GitLab.com

#### Docker Machine - it can bite

[Unexpected upgrade of Docker Engine breaks autoscaled Runners](https://gitlab.com/gitlab-com/infrastructure/issues/957)

- fix systemd service file
- Packer and own images to prevent from this in the future


+++

### The story of CI at GitLab.com

#### Docker Machine - it can bite

Further usage and outages ended with more updates sent to Docker Machine:

- [404 is a success when deleting GCE machine and disk](https://github.com/docker/machine/pull/4330)
- [Make DigitalOcean driver RateLimit aware](https://github.com/docker/machine/pull/4331)

+++

### The story of CI at GitLab.com

#### Docker Machine - it can bite

[But sometimes it ends without explanation](https://gitlab.com/gitlab-com/infrastructure/issues/3427)

---

### The story of CI at GitLab.com

#### Cost and performance optimisations

[Tune created machines resources and introduce gitlab-shared-runners-manager-X Runners](https://gitlab.com/gitlab-com/infrastructure/issues/688)
to improve performance and reduce costs of CI on GitLab.com

+++

### The story of CI at GitLab.com

#### Cost and performance optimisations

- [the story of _high-CPU_ droplets](https://gitlab.com/gitlab-com/infrastructure/issues/2046) for GitLab CE/EE jobs
- and [GCP way to _high-CPU_ replacement](https://gitlab.com/gitlab-com/infrastructure/issues/3455)

+++

### The story of CI at GitLab.com

#### Cost and performance optimisations

[Off-peak for Shared Runners](https://gitlab.com/gitlab-com/infrastructure/issues/2569)

```
"OffPeakPeriods": [
  "* * * * * sat,sun *"
],
"OffPeakTimezone": "UTC",
"OffPeakIdleCount": 5,
"OffPeakIdleTime": 1800,
```

+++

### The story of CI at GitLab.com

#### Cost and performance optimisations

GitLab Workhorse:

- passing traces, artifacts without Rails
- currently also proxy between user/Runner and object storage
- queueing: the solution, the problem and metrics to the rescue

---

### The story of CI at GitLab.com

#### Monitoring 2.0 - Prometheus and Grafana

[Introduce grafana dashboard for CI](https://gitlab.com/gitlab-com/infrastructure/issues/840)

- Prometheus as metrics collector and alerting mechanism
- Grafana for visualisation
- publicly available CI dashboards:
    - https://monitor.gitlab.net/dashboard/db/ci?theme=light
    - https://monitor.gitlab.net/dashboard/db/ci-autoscaled-machines-metrics?theme=light
    - https://monitor.gitlab.net/dashboard/db/ci-autoscaling-providers?theme=light

+++

### The story of CI at GitLab.com

#### Monitoring 2.0 - Prometheus and Grafana

![Overview of main CI dashboard in Grafana](img/grafana-ci-dashboard-overview.png)

+++

### The story of CI at GitLab.com

#### Monitoring 2.0 - Prometheus and Grafana

Prometheus gives also alerting (way more powerful than we had with check_mk):

```
rules:
  - alert: CICDTooManyPendingBuildsOnSharedRunnerProject
    expr: (ci_pending_builds{has_minutes="yes",shared_runners="yes"} > 500) and (topk(1,
      predict_linear(ci_pending_builds{has_minutes="yes",shared_runners="yes"}[15m], 3600)) > 1000)
    for: 5m
```

+++

### The story of CI at GitLab.com

#### Monitoring 2.0 - Prometheus and Grafana

E2E monitoring of autoscaled machines odyssey:

- started almost year ago (end of April 2017)
- [Prometheus performance](https://gitlab.com/gitlab-com/infrastructure/issues/1640)
- [Consul configuration problems](https://gitlab.com/gitlab-com/infrastructure/issues/1639)
- Prometheus 2.0 and 2.1 for the rescue
- exporters tuning

+++

### The story of CI at GitLab.com

#### Monitoring 2.0 - Prometheus and Grafana

![Overview of machines E2E monitoring dashboard in Grafana](img/e2e-machines-monitoring-dashboard-overview.png)

---

### The story of CI at GitLab.com

#### Outages and how we're handling them

[A really bad one, when backup environment couldn't start](https://gitlab.com/gitlab-com/infrastructure/issues/1838)

- mess after previous usage of backup environment introduced problems with using it when needed
- cloud provider problem ended with manual queue handling for several hours

+++

### The story of CI at GitLab.com

#### Outages and how we're handling them

_StuckCiJobsWorker_ - [the outage that doesn't hammer users but makes you blind](https://gitlab.com/gitlab-com/infrastructure/issues/3866):

- _StuckCiJobsWorker_ - the script that removes staled jobs
- auto-retry - brilliant solution! Automatize manual work for a user, whenever it's possible!
- protected jobs - security feature!
- together they killed _StuckCiJobsWorker_ on GitLab.com for almost three weeks!

+++

### The story of CI at GitLab.com

#### Outages and how we're handling them

[DB concurrency problems affecting jobs queue](https://gitlab.com/gitlab-com/infrastructure/issues/1106);
just before Team-Member-1 pressed _[enter]_ at 2017-02-28 ;)

Conclusion: DB may very easy become a blocker for GitLab CI

---

### The story of CI at GitLab.com

#### Cache servers saga

- introduced with autoscaling
    - cache server - stores cache generated by GitLab Runner in user's job
    - needed, because in autoscaled repository there is no _local_ storage where cache can be stored
- [shared between runners](https://gitlab.com/gitlab-com/infrastructure/issues/1780) after few months
- [monitoring improvements](https://gitlab.com/gitlab-com/infrastructure/issues/2116) after a crash

+++

### The story of CI at GitLab.com

#### Cache servers saga

[A cache machine that exploded](https://gitlab.com/gitlab-com/infrastructure/issues/1964)
+++

### The story of CI at GitLab.com

#### Cache servers saga

- ARP, something that web developers usually [don't need to think about](https://gitlab.com/gitlab-com/infrastructure/issues/2101)...

    ```
    "sysctl": {
      "params": {
        "net": {
          "ipv4": {
            "neigh": {
              "default": {
                "gc_interval": 60,
                "gc_stale_time": 120,
                "gc_thresh3": 4096,
                "gc_thresh2": 2048,
                "gc_thresh1": 1024
              }
            }
          }
        }
      }
    }
    ```

- but they should [be familiar with nginx buffers!](https://gitlab.com/gitlab-com/infrastructure/issues/2102)

    ```
    proxy_request_buffering off;
    ```

+++

### The story of CI at GitLab.com

#### Cache servers saga

[RAID-0 on SAN devices](https://gitlab.com/gitlab-com/infrastructure/issues/2165)... hope it will work is the only explanation of configuring it ;)

+++

### The story of CI at GitLab.com

#### Cache servers saga

[Cache cleaning cron and why consistent _chef_ usage is important](https://gitlab.com/gitlab-com/infrastructure/issues/3440)

- retrieved ~3TB after cleaning old files

+++

### The story of CI at GitLab.com

#### Cache servers saga

[Sometimes you don't know what happened, and reboot is the only solution](https://gitlab.com/gitlab-com/infrastructure/issues/3157)

And again a change that was not added to _chef_ was a problem!

---

### The story of CI at GitLab.com

#### Abusers

We're open, but not here; so sorry, no details...

... well, almost no details :) <!-- .element: class="fragment" data-fragment-index="1" -->

+++

### The story of CI at GitLab.com

#### Abusers

Free service will always be abused. Be prepared!

+++

### The story of CI at GitLab.com

#### Abusers

Gather data to detect abusers

+++

### The story of CI at GitLab.com

#### Abusers

Gather data to respond for abuse reports

+++

### The story of CI at GitLab.com

#### Abusers

How to prevent?

- set limits
- maybe a detection AI?
- automation/tooling for abuse handling to reduce your workload

---

## Where are we now?

---

### Where are we now?

#### Numbers!

- connected runners:
    - specific: **14469**
    - shared: **8**
- jobs handled daily:
    - 06.03: 221443
    - 07.03: 210372
    - 08.03: 174527
    - 09.03: 154473
    - 10.03 (Saturday): 63576
    - 11.03 (Sunday): 62648
    - 12.03: 191881
    - 13.03: 201509
    - Average - 150k-200k per day, half of this at weekends.

- Real-life graphs from Grafana Dashboards:
    - https://monitor.gitlab.net/dashboard/db/ci?theme=light
    - https://monitor.gitlab.net/dashboard/db/ci-autoscaled-machines-metrics?theme=light
    - https://monitor.gitlab.net/dashboard/db/ci-autoscaling-providers?theme=light

+++

### Where are we now?

#### Current architecture overview

![current infrastructure of GitLab.com](img/current-gitlab-com-infrastructure.png)

+++

### Where are we now?

#### This how it looked two years ago

![infrastructure when autoscaling was introduced](img/infrastructure-with-introduced-autoscaling.png)

+++

### Where are we now?

#### Runner's cluster architecture overview

![runners cluster general architecture](img/runners-general-architecture.png)

+++

### Where are we now?

#### Runner's cluster architecture inside of cloud provider

![runners cluster general architecture](img/runners-architecture-in-cloud-provider.png)

+++

### Where are we now?

#### Team

- testing the SRE concept - one person that focuses on reliability and stability issues
- production team with ops background to help
- GitLab Runner maintainers for managing the Runner's fleet and configuration
- CI/CD team developers to help production team with other parts of CI infrastructure

+++

### Where are we now?

[Runbooks](https://gitlab.com/gitlab-com/runbooks)

- Troubleshooting:
    - Introduction to Shared Runners
    - Understand CI graphs
    - Large number of CI pending builds
    - The CI runner manager report a high DO Token Rate Limit usage
    - The CI runner manager report a high number of errors
    - Runners cache is down
    - Runners registry is down
    - Runners cache free disk space is less than 20%
    - Too many connections on Runner's cache server
- Work with CI Infrastructure:
    - Update GitLab Runner on runners managers
    - Investigate Abuse Reports
    - Create runners manager for GitLab.com
    - Update docker-machine
    - CI project namespace check

+++

### Where are we now?

#### Tooling

- [hanging-droplets-cleaner](https://gitlab.com/tmaczukin/hanging-droplets-cleaner) (to be moved to [gitlab-org/ci-cd](https://gitlab.com/gitlab-org/ci-cd/) group)
- [doplet-zero-cleaner](https://gitlab.com/tmaczukin/droplet-zero-machines-cleaner) (to be moved to [gitlab-org/ci-cd](https://gitlab.com/gitlab-org/ci-cd/) group)
- [digitalocean-exporter](https://github.com/andrewsomething/digitalocean_exporter/)
- [gcp-exporter](https://gitlab.com/gitlab-org/ci-cd/gcp-exporter)
- ops-helper (private, at least for now)
- /runners-X COG commands

+++

### Where are we now?

#### Monitoring

- Prometheus (for Runners, and Prometheus+Consul cluster for autoscaled machines)
- Prometheus alerts integrated with Slack (and _cicdops_ group on Slack to mention people directly)
- Grafana for visualisation
- Sentry for gathering exception
- ELK for logs processing (however ascetic _grep_ knowledge still rules!)

+++

### Where are we now?

#### Production settings

Some of the settings that we're using for Production:

- [General CI/CD settings](https://docs.gitlab.com/ee/user/gitlab_com/#gitlab-ci-cd)
- [Cron jobs](https://docs.gitlab.com/ee/user/gitlab_com/#cron-jobs) (includes pipelines scheduler)
- [Shared Runners configuration](https://docs.gitlab.com/ee/user/gitlab_com/#shared-runners)

---

## Looking into the future

---

### Looking into the future

Move CI/CD jobs queueing out of DB:

- one of the options - put it in Redis
- another one (may be connected with the first one) prepare a specialized daemon for CI, like we've done with Gitaly for Git?

+++

### Looking into the future

Polling - is it the best solution for Runner -> GitLab communication?

- pull vs. push and problems of corporate networks and personal laptops
- maybe websockets?

+++

### Looking into the future

Object Storage:

- move artifacts to O/S - done!
- move job traces to O/S - almost there!
- direct upload from Runner to O/S - on the roadmap

+++

### Looking into the future

Runner Managers deployment using k8s:

- faster
- simpler

---

## Q&A

